# Catch 'em all

HTML5 Brick Breaker Game with Pokémon theme

This game is an example project to discover HTML5 canvas and Firebase as a backend technology. 
It consists of a local highscore in the top left corner and the main playing area in the middle of the screen. 
The player is able to pause the game, reach the final level 10 (hardest level too) or most likely lose all three attempts in the process of achieving the highscore.

![Gameplay](assets/images/Catch_em_all_demobild.png)

## Infrastructure
Made with pure HTML5, CSS3 and native JS. Implemented Firebase as backend technology.

## Future
More features to come. 

*Ball speed could increase over time and with difficulty.
*Login screen for different local highscores
*Overview of different local highscores
*Login with googlemail account
*Customized background
*Ingame currency (coins) for playing the game
*Ingame shop to buy ball / paddle skins and power-ups
*Multilingual
*Random power-ups while playing the game
*Statistics for amount specific pokemon caught
