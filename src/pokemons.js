import { getRandomNumberBetween } from '/src/util.js';

export const pokemons = [
    '/assets/images/pokemons/abra.png',
    '/assets/images/pokemons/bellsprout.png',
    '/assets/images/pokemons/bulbasaur.png',
    '/assets/images/pokemons/caterpie.png',
    '/assets/images/pokemons/charmander.png',
    '/assets/images/pokemons/dratini.png',
    '/assets/images/pokemons/eevee.png',
    '/assets/images/pokemons/jigglypuff.png',
    '/assets/images/pokemons/mankey.png',
    '/assets/images/pokemons/meowth.png',
    '/assets/images/pokemons/mew.png',
    '/assets/images/pokemons/pidgey.png',
    '/assets/images/pokemons/pikachu.png',
    '/assets/images/pokemons/psyduck.png',
    '/assets/images/pokemons/rattata.png',
    '/assets/images/pokemons/snorlax.png',
    '/assets/images/pokemons/squirtle.png',
    '/assets/images/pokemons/venonat.png',
    '/assets/images/pokemons/weedle.png',
    '/assets/images/pokemons/zubat.png'
];

export function getRandomPokemon() {
    return pokemons[getRandomNumberBetween(0, pokemons.length - 1)];
}