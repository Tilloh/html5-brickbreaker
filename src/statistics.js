export function isLocalHighscorePresent() {
    return localStorage.getItem('localHighscore') ? true : false;
}

export function getLocalHighscore() {
    return JSON.parse(localStorage.getItem('localHighscore'));
}

export function setLocalHighscore(newHighscore) {
    localStorage.setItem('localHighscore', JSON.stringify(newHighscore));
}