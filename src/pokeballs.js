import { getRandomNumberBetween } from '/src/util.js';

export const pokeballs = [
    '/assets/images/pokeballs/pokeball.png',
    '/assets/images/pokeballs/super_ball.png',
    '/assets/images/pokeballs/ultra_ball.png',
    '/assets/images/pokeballs/master_ball.png'
];

export function getRandomPokeball() {
    return pokeballs[getRandomNumberBetween(0, pokeballs.length - 1)];
}
