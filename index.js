import Game from "/src/game.js";
import {
    isLocalHighscorePresent,
    getLocalHighscore,
    setLocalHighscore
} from '/src/statistics.js';

let canvas = document.getElementById("gameScreen");
let ctx = canvas.getContext("2d");

const GAME_WIDTH = 400;
const GAME_HEIGHT = 800;

// init canvas
canvas.setAttribute("width", GAME_WIDTH);
canvas.setAttribute("height", GAME_HEIGHT);

// init game
let game = new Game(GAME_WIDTH, GAME_HEIGHT);

let lastTime = 0;

// runs every frame
function gameLoop(timestamp) {
    let deltaTime = timestamp - lastTime;
    lastTime = timestamp;

    // clear canvas
    ctx.clearRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
    
    game.update(deltaTime);
    game.draw(ctx);

    // update visual highscore
    updateVisualHighscore();

    requestAnimationFrame(gameLoop);
}

// game start
requestAnimationFrame(gameLoop);

function updateVisualHighscore() {
    let highscoreContainer = document.getElementById('highscoreContainer');
    let nolocalHighscoreText = document.getElementById('nolocalHighscore');

    if(!isLocalHighscorePresent()) {
        highscoreContainer.style.display = 'none';
        nolocalHighscoreText.style.display = 'block';
    } else {
        highscoreContainer.style.display = 'block';
        nolocalHighscoreText.style.display = 'none';
        let highestLevel = document.getElementById('highestLevel');
        let highestCaught = document.getElementById('highestCaught');
        let highestKeptUp = document.getElementById('highestKeptUp');
        let highestLifes = document.getElementById('highestLifes');
        let localHighscore = getLocalHighscore();

        highestLevel.innerText = localHighscore.level;
        highestCaught.innerText = localHighscore.caught;
        highestKeptUp.innerText = localHighscore.keptUp;
        highestLifes.innerText = localHighscore.lifes;
    }
}
